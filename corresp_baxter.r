JRA1 <- read.csv("ca.csv", header=TRUE)
library(MASS)
JRAca1 <- corresp(JRA1, nf=2)
biplot(JRAca1)
date <- c(1,1,1,1,1,1,1,1,2,2,4,4,4,4,4,4,4,4)
biplot(JRAca1, xlabs=date)

type = c("C", "Bw", "Ja", "F", "Ju", "Bt")
par(mfrow = c(1,2))
biplot(JRAca1, xlabs = date, ylabs = rep(" ", 6))
biplot(JRAca1, xlabs = rep("", 18), ylabs = type)
par(mfrow = c(1,1))
library(ade4)
library(vegan)


flavian <- read.csv("flavian.csv", header=T)

flavianca <- corresp(flavian)
plot(flavianca)
biplot(flavianca)

flavianca <- corresp(flavian, nf=2)
biplot(flavianca)


gqb <- read.csv("ceramica-quant.csv", header=T)
gqb
gqb[,c(2:5)]
gqbca <- corresp(gqb[,c(2:5)])
gqbca <- corresp(gqb[,c(2:5)], nf=2)
biplot(gqbca)
biplot(gqbca, xlabs=gqb[,1])

